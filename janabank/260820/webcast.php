<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $emailid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$emailid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_emailid"]);
            unset($_SESSION["user_empid"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div class="container-fluid">
    <div class="row bg-white p-3" >
        <div class="col-8 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-2">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
    </div>
    <div class="row p-3" >
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center">
            <h4 class="text-uppercase">Live Webcasting with Ajay Kanwal</h4>
        </div>
    </div>    
    <div class="row mt-1 mb-1">
        <div class="col-10 offset-1 text-right">
            <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    <div class="row video-panel">
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no"></iframe>
            </div>    
        </div>
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>