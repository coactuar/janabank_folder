<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container-fluid">
    <div class="row bg-white p-3" >
        <div class="col-8 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-2">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
    </div>
    <div class="row p-3" >
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center">
            <h4 class="text-uppercase">Live Webcast with Ajay Kanwal</h4>
        </div>
    </div>    
    <div class="row video-panel">
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no"></iframe>
            </div>    
        </div>
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-23"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-23');
</script>

</body>
</html>